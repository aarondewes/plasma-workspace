# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Geoffray Levasseur <geoffray.levasseurbrandin@numericable.fr>, 2014.
# Vincent PINON <vpinon@april.org>, 2014.
# Vincent Pinon <vpinon@kde.org>, 2017.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
# Xavier BESNARD <xavier.besnard@neuf.fr>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-09 01:04+0000\n"
"PO-Revision-Date: 2023-02-14 21:31+0100\n"
"Last-Translator: Xavier BESNARD <xavier.besnard]neuf.fr>\n"
"Language-Team: fr\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.2\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "Ouvrir le dossier contenant"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audios"
msgstr "Audios"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Images"
msgstr "Images"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Videos"
msgstr "Vidéos"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheets"
msgstr "Feuilles de calcul"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentations"
msgstr "Présentations"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folders"
msgstr "Dossiers"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Documents"
msgstr "Documents"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archives"
msgstr "Archives"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Texts"
msgstr "Textes"

#: baloosearchrunner.cpp:96
#, kde-format
msgid "Files"
msgstr "Fichiers"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Rechercher dans les fichiers, courriels et contacts"

#~ msgid "Email"
#~ msgstr "Courrier électronique"
